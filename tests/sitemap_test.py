import os
from sitemapparser.parser import SitemapParser
import unittest


def _read_test(filename: str) -> str:
    with open(os.path.join('examples', filename)) as file:
        return file.read()


class IndexTests(unittest.TestCase):
    def test_bloomberg_index(self):
        sitemap = _read_test('bloomberg_index.xml')
        parser = SitemapParser(sitemap)
        self.assertIsNotNone(parser.items())
        self.assertTrue(len(parser.items()) == 339)

    def test_cnbc_index(self):
        sitemap = _read_test('cnbc_index.xml')
        parser = SitemapParser(sitemap)
        self.assertIsNotNone(parser.items())
        self.assertTrue(len(parser.items()) == 6)

class NewsTests(unittest.TestCase):
    def test_usnews_news(self):
        sitemap = _read_test('usnews_news.xml')
        parser = SitemapParser(sitemap)
        self.assertIsNotNone(parser.items())
        self.assertTrue(len(parser.items()) == 918)


class UrlSet(unittest.TestCase):
    def test_mcclatchydc_urlset(self):
        sitemap = _read_test('mcclatchydc_urlset.xml')
        parser = SitemapParser(sitemap)
        self.assertIsNotNone(parser.items())
        self.assertTrue(len(parser.items()) == 1000)


if __name__ == '__main__':
    unittest.main()
