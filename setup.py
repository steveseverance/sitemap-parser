from setuptools import setup

setup(
   name='sitemap-parser',
   version='1.0',
   description='A parser for sitemaps based on BeautifulSoup and LibXML',
   author='Steve Severance',
   author_email='steve@prescriptiveintelligence.com',
   packages=['sitemapparser'],  #same as name
   install_requires=['beautifulsoup4', 'dateparser', 'lxml'], #external packages as dependencies
)