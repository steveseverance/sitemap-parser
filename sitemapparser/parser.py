from bs4 import BeautifulSoup, Tag
import dateparser
from datetime import datetime
from typing import List, Optional


class News:
    def __init__(self, publication, language, genres, pubdate, title, keywords, stocktickers):
        self.publication_name = publication
        self.publication_language = language
        self.genres = genres
        self.pubdate = pubdate
        self.title = title
        self.keywords = keywords
        self.stock_tickers = stocktickers


class SitemapItem:
    def __init__(self, loc, lastmod, news=None):
        self.loc = loc
        self.lastmod = lastmod
        self.news = news


class IndexSitemapItem(SitemapItem):
    def __init__(self, loc, lastmod):
        super(IndexSitemapItem, self).__init__(loc, lastmod)


class SitemapParser:
    def __init__(self, sitemap: str):
        self._soup = BeautifulSoup(sitemap, "lxml-xml")
        self._items = []
        self.dateparser = dateparser.DateDataParser(languages=['en'], settings={'RETURN_AS_TIMEZONE_AWARE': True})
        self._parse()

    def _parse(self):
        if self._soup.indexes:
            for x in self._soup.indexes.children:
                if isinstance(x, Tag):
                    loc = x.loc.text
                    lastmod = self._get_lastmod(x)
                    self._items.append(IndexSitemapItem(loc, lastmod))
        if self._soup.sitemapindex:
            for x in self._soup.sitemapindex.children:
                if isinstance(x, Tag):
                    loc = x.loc.text
                    lastmod = self._get_lastmod(x)
                    self._items.append(IndexSitemapItem(loc, lastmod))
        elif self._soup.urlset:
            for x in self._soup.urlset.children:
                if isinstance(x, Tag):
                    loc = x.loc.text
                    lastmod = self._get_lastmod(x)
                    if x.news:
                        news = self._get_news(x.news)
                    else:
                        news = None
                    self._items.append(SitemapItem(loc, lastmod, news))

    def _get_lastmod(self, tag: Tag) -> Optional[datetime]:
        if tag.lastmod:
            return self.dateparser.get_date_data(tag.lastmod.text)['date_obj']

    def _get_tag_text(self, x: Tag) -> str:
        if x and isinstance(x, Tag):
            return x.text
        elif isinstance(x, str):
            return x

    def _get_news(self, tag: Tag) -> News:
        pub_name = self._get_tag_text(tag.find('name'))
        pub_language = self._get_tag_text(tag.publication.language)
        genres = self._get_tag_text(tag.genres)
        pubdate = self._get_tag_text(tag.publication_date)
        if pubdate:
            pubdate = self.dateparser.get_date_data(pubdate)['date_obj']
        title = self._get_tag_text(tag.title)
        keywords = self._get_tag_text(tag.keywords)
        stock_tickers = self._get_tag_text(tag.stock_tickers)
        return News(pub_name, pub_language, genres, pubdate, title, keywords, stock_tickers)

    def items(self) -> List[SitemapItem]:
        return self._items

